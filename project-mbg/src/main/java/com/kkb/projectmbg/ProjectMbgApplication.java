package com.kkb.projectmbg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectMbgApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectMbgApplication.class, args);
    }

}
