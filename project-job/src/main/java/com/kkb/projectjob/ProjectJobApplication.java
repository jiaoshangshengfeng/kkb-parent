package com.kkb.projectjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectJobApplication.class, args);
    }

}
