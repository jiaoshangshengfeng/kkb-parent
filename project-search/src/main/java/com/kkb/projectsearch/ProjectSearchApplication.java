package com.kkb.projectsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSearchApplication.class, args);
    }

}
