package com.kkb.projectcommon.exception;

import com.kkb.projectcommon.api.IErrorCode;

/**
 * @Description: 断言处理类，用于抛出各种API异常
 * @author: peng.ni
 * @date: 2021/04/07
 */
public class Asserts {
    public static void fail(String message) {
        throw new ApiException(message);
    }

    public static void fail(IErrorCode errorCode) {
        throw new ApiException(errorCode);
    }
}
